package com.example.kudaki.model;

public interface iUser {
    String getName();
    String getEmail();
    String getPassword();
}
