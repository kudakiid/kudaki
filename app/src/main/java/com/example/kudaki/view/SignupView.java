package com.example.kudaki.view;

public interface SignupView {
    void onSignupResult(String message);
}
