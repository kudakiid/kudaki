package com.example.kudaki.view;

public interface SigninView {
    void onSigninResult(String message);
}
