package com.example.kudaki.presenter.auth.login;

public interface SigninPresenter {
    void onSignin(String email, String password);
}
