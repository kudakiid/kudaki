package com.example.kudaki.presenter.auth.signup;

public interface SignupPresenter {
    void onSignup(String name, String email, String password);
}
